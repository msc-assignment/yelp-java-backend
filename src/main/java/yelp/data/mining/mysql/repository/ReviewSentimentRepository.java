package yelp.data.mining.mysql.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import yelp.data.mining.mysql.entity.Business;
import yelp.data.mining.mysql.entity.ReviewSentiment;

@RepositoryRestResource(exported = false)
public interface ReviewSentimentRepository extends JpaRepository<ReviewSentiment, Long> {

  public List<ReviewSentiment> findByBusinessIn(final List<Business> businesses);

}
