package yelp.data.mining.mysql.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import yelp.data.mining.mysql.entity.Business;

@RepositoryRestResource(exported = false)
public interface BusinessRepository extends JpaRepository<Business, String> {

  public List<Business> findByIdIn(final List<String> businessIds);

}
