package yelp.data.mining.mysql.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import yelp.data.mining.mysql.entity.Review;

@RepositoryRestResource(exported = false)
public interface ReviewRepository extends JpaRepository<Review, String> {

  public Page<Review> findByIsProcessed(final Boolean isProcessed, final Pageable page);

}
