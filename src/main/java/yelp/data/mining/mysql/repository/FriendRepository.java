package yelp.data.mining.mysql.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import yelp.data.mining.mysql.entity.Friend;
import yelp.data.mining.mysql.entity.id.FriendId;

@RepositoryRestResource(exported = false)
public interface FriendRepository extends JpaRepository<Friend, FriendId> {

  public List<Friend> findByUserId(final String userId);

}
