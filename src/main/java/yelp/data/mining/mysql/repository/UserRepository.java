package yelp.data.mining.mysql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import yelp.data.mining.mysql.entity.User;

@RepositoryRestResource(exported = false)
public interface UserRepository extends JpaRepository<User, String> {

}
