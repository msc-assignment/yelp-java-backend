package yelp.data.mining.mysql.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

  public static class ColumnKeys {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String REVIEW_COUNT = "review_count";
    private static final String YELPING_SINCE = "yelping_since";
    private static final String USEFUL = "useful";
    private static final String FUNNY = "funny";
    private static final String COOL = "cool";
    private static final String FANS = "fans";
    private static final String AVERAGE_STARS = "average_stars";
    private static final String COMPLIMENT_HOT = "compliment_hot";
    private static final String COMPLIMENT_MORE = "compliment_more";
    private static final String COMPLIMENT_PROFILE = "compliment_profile";
    private static final String COMPLIMENT_CUTE = "compliment_cute";
    private static final String COMPLIMENT_LIST = "compliment_list";
    private static final String COMPLIMENT_NOTE = "compliment_note";
    private static final String COMPLIMENT_PLAIN = "compliment_plain";
    private static final String COMPLIMENT_COOL = "compliment_cool";
    private static final String COMPLIMENT_FUNNY = "compliment_funny";
    private static final String COMPLIMENT_WRITER = "compliment_writer";
    private static final String COMPLIMENT_PHOTOS = "compliment_photos";
  }

  public static class JsonKeys {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String REVIEW_COUNT = "reviewCount";
    private static final String YELPING_SINCE = "yelpingSince";
    private static final String USEFUL = "useful";
    private static final String FUNNY = "funny";
    private static final String COOL = "cool";
    private static final String FANS = "fans";
    private static final String AVERAGE_STARS = "averageStars";
    private static final String COMPLIMENT_HOT = "complimentHot";
    private static final String COMPLIMENT_MORE = "complimentMore";
    private static final String COMPLIMENT_PROFILE = "complimentProfile";
    private static final String COMPLIMENT_CUTE = "complimentCute";
    private static final String COMPLIMENT_LIST = "complimentList";
    private static final String COMPLIMENT_NOTE = "complimentNote";
    private static final String COMPLIMENT_PLAIN = "complimentPlain";
    private static final String COMPLIMENT_COOL = "complimentCool";
    private static final String COMPLIMENT_FUNNY = "complimentFunny";
    private static final String COMPLIMENT_WRITER = "complimentWriter";
    private static final String COMPLIMENT_PHOTOS = "complimentPhotos";
    private static final String FRIENDS = "friends";

  }

  @Id
  @Column(name = ColumnKeys.ID)
  @JsonProperty(JsonKeys.ID)
  private String id;

  @Column(name = ColumnKeys.NAME)
  @JsonProperty(JsonKeys.NAME)
  private String name;

  @Column(name = ColumnKeys.REVIEW_COUNT)
  @JsonProperty(JsonKeys.REVIEW_COUNT)
  private Long reviewCount;

  @Column(name = ColumnKeys.YELPING_SINCE)
  @JsonProperty(JsonKeys.YELPING_SINCE)
  private Date yelpingSince;

  @Column(name = ColumnKeys.USEFUL)
  @JsonProperty(JsonKeys.USEFUL)
  private Long useful;

  @Column(name = ColumnKeys.FUNNY)
  @JsonProperty(JsonKeys.FUNNY)
  private Long funny;

  @Column(name = ColumnKeys.COOL)
  @JsonProperty(JsonKeys.COOL)
  private Long cool;

  @Column(name = ColumnKeys.FANS)
  @JsonProperty(JsonKeys.FANS)
  private Long fans;

  @Column(name = ColumnKeys.AVERAGE_STARS)
  @JsonProperty(JsonKeys.AVERAGE_STARS)
  private Float averageStars;

  @Column(name = ColumnKeys.COMPLIMENT_HOT)
  @JsonProperty(JsonKeys.COMPLIMENT_HOT)
  private Long complimentHot;

  @Column(name = ColumnKeys.COMPLIMENT_MORE)
  @JsonProperty(JsonKeys.COMPLIMENT_MORE)
  private Long complimentMore;

  @Column(name = ColumnKeys.COMPLIMENT_PROFILE)
  @JsonProperty(JsonKeys.COMPLIMENT_PROFILE)
  private Long complimentProfile;

  @Column(name = ColumnKeys.COMPLIMENT_CUTE)
  @JsonProperty(JsonKeys.COMPLIMENT_CUTE)
  private Long complimentCute;

  @Column(name = ColumnKeys.COMPLIMENT_LIST)
  @JsonProperty(JsonKeys.COMPLIMENT_LIST)
  private Long complimentList;

  @Column(name = ColumnKeys.COMPLIMENT_NOTE)
  @JsonProperty(JsonKeys.COMPLIMENT_NOTE)
  private Long complimentNote;

  @Column(name = ColumnKeys.COMPLIMENT_PLAIN)
  @JsonProperty(JsonKeys.COMPLIMENT_PLAIN)
  private Long complimentPlain;

  @Column(name = ColumnKeys.COMPLIMENT_COOL)
  @JsonProperty(JsonKeys.COMPLIMENT_COOL)
  private Long complimentCool;

  @Column(name = ColumnKeys.COMPLIMENT_FUNNY)
  @JsonProperty(JsonKeys.COMPLIMENT_FUNNY)
  private Long complimentFunny;

  @Column(name = ColumnKeys.COMPLIMENT_WRITER)
  @JsonProperty(JsonKeys.COMPLIMENT_WRITER)
  private Long complimentWriter;

  @Column(name = ColumnKeys.COMPLIMENT_PHOTOS)
  @JsonProperty(JsonKeys.COMPLIMENT_PHOTOS)
  private Long complimentPhotos;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getReviewCount() {
    return reviewCount;
  }

  public void setReviewCount(Long reviewCount) {
    this.reviewCount = reviewCount;
  }

  public Date getYelpingSince() {
    return yelpingSince;
  }

  public void setYelpingSince(Date yelpingSince) {
    this.yelpingSince = yelpingSince;
  }

  public Long getUseful() {
    return useful;
  }

  public void setUseful(Long useful) {
    this.useful = useful;
  }

  public Long getFunny() {
    return funny;
  }

  public void setFunny(Long funny) {
    this.funny = funny;
  }

  public Long getCool() {
    return cool;
  }

  public void setCool(Long cool) {
    this.cool = cool;
  }

  public Long getFans() {
    return fans;
  }

  public void setFans(Long fans) {
    this.fans = fans;
  }

  public Float getAverageStars() {
    return averageStars;
  }

  public void setAverageStars(Float averageStars) {
    this.averageStars = averageStars;
  }

  public Long getComplimentHot() {
    return complimentHot;
  }

  public void setComplimentHot(Long complimentHot) {
    this.complimentHot = complimentHot;
  }

  public Long getComplimentMore() {
    return complimentMore;
  }

  public void setComplimentMore(Long complimentMore) {
    this.complimentMore = complimentMore;
  }

  public Long getComplimentProfile() {
    return complimentProfile;
  }

  public void setComplimentProfile(Long complimentProfile) {
    this.complimentProfile = complimentProfile;
  }

  public Long getComplimentCute() {
    return complimentCute;
  }

  public void setComplimentCute(Long complimentCute) {
    this.complimentCute = complimentCute;
  }

  public Long getComplimentList() {
    return complimentList;
  }

  public void setComplimentList(Long complimentList) {
    this.complimentList = complimentList;
  }

  public Long getComplimentNote() {
    return complimentNote;
  }

  public void setComplimentNote(Long complimentNote) {
    this.complimentNote = complimentNote;
  }

  public Long getComplimentPlain() {
    return complimentPlain;
  }

  public void setComplimentPlain(Long complimentPlain) {
    this.complimentPlain = complimentPlain;
  }

  public Long getComplimentCool() {
    return complimentCool;
  }

  public void setComplimentCool(Long complimentCool) {
    this.complimentCool = complimentCool;
  }

  public Long getComplimentFunny() {
    return complimentFunny;
  }

  public void setComplimentFunny(Long complimentFunny) {
    this.complimentFunny = complimentFunny;
  }

  public Long getComplimentWriter() {
    return complimentWriter;
  }

  public void setComplimentWriter(Long complimentWriter) {
    this.complimentWriter = complimentWriter;
  }

  public Long getComplimentPhotos() {
    return complimentPhotos;
  }

  public void setComplimentPhotos(Long complimentPhotos) {
    this.complimentPhotos = complimentPhotos;
  }

}
