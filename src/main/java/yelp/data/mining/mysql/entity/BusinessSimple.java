package yelp.data.mining.mysql.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "business")
public class BusinessSimple {

  public static class ColumnKeys {

    private static final String ID = "id";
    private static final String NAME = "name";
  }

  public static class JsonKeys {

    private static final String ID = "id";
    private static final String NAME = "name";
  }

  @Id
  @Column(name = ColumnKeys.ID)
  @JsonProperty(JsonKeys.ID)
  private String id;

  @Column(name = ColumnKeys.NAME)
  @JsonProperty(JsonKeys.NAME)
  private String name;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
