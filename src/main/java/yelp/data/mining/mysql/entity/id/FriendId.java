package yelp.data.mining.mysql.entity.id;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;

@Embeddable
public class FriendId implements Serializable {

  private String userId;
  private String friendId;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getFriendId() {
    return friendId;
  }

  public void setFriendId(String friendId) {
    this.friendId = friendId;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 37 * hash + Objects.hashCode(this.userId);
    hash = 37 * hash + Objects.hashCode(this.friendId);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final FriendId other = (FriendId) obj;
    if (!Objects.equals(this.userId, other.userId)) {
      return false;
    }
    if (!Objects.equals(this.friendId, other.friendId)) {
      return false;
    }
    return true;
  }

}
