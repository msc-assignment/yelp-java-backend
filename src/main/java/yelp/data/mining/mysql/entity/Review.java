package yelp.data.mining.mysql.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "review")
public class Review {

  public static class ColumnKeys {

    private static final String ID = "id";
    private static final String STARS = "stars";
    private static final String DATE = "date";
    private static final String TEXT = "text";
    private static final String USEFUL = "useful";
    private static final String FUNNY = "funny";
    private static final String COOL = "cool";
    public static final String BUSINESS_ID = "business_id";
    public static final String USER_ID = "user_id";
    public static final String PROCESSED = "processed";
  }

  public static class JsonKeys {

    private static final String ID = "id";
    private static final String STARS = "stars";
    private static final String DATE = "date";
    private static final String TEXT = "text";
    private static final String USEFUL = "useful";
    private static final String FUNNY = "funny";
    private static final String COOL = "cool";
  }

  @Id
  @Column(name = ColumnKeys.ID)
  @JsonProperty(JsonKeys.ID)
  private String id;

  @Column(name = ColumnKeys.STARS)
  @JsonProperty(JsonKeys.STARS)
  private Long stars;

  @Column(name = ColumnKeys.DATE)
  @JsonProperty(JsonKeys.DATE)
  private Date date;

  @Column(name = ColumnKeys.TEXT)
  @JsonProperty(JsonKeys.TEXT)
  private String text;

  @Column(name = ColumnKeys.USEFUL)
  @JsonProperty(JsonKeys.USEFUL)
  private Long useful;

  @Column(name = ColumnKeys.FUNNY)
  @JsonProperty(JsonKeys.FUNNY)
  private Long funny;

  @Column(name = ColumnKeys.COOL)
  @JsonProperty(JsonKeys.COOL)
  private Long cool;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = ColumnKeys.BUSINESS_ID)
  @JsonIgnore
  private Business business;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = ColumnKeys.USER_ID)
  @JsonIgnore
  private User user;

  @Column(name = ColumnKeys.PROCESSED)
  @JsonIgnore
  private Boolean isProcessed;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Long getStars() {
    return stars;
  }

  public void setStars(Long stars) {
    this.stars = stars;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Long getUseful() {
    return useful;
  }

  public void setUseful(Long useful) {
    this.useful = useful;
  }

  public Long getFunny() {
    return funny;
  }

  public void setFunny(Long funny) {
    this.funny = funny;
  }

  public Long getCool() {
    return cool;
  }

  public void setCool(Long cool) {
    this.cool = cool;
  }

  public Business getBusiness() {
    return business;
  }

  public void setBusiness(Business business) {
    this.business = business;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Boolean getIsProcessed() {
    return isProcessed;
  }

  public void setIsProcessed(Boolean isProcessed) {
    this.isProcessed = isProcessed;
  }

}
