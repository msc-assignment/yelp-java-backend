package yelp.data.mining.mysql.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;

public class BusinessReview {

  public static class JsonKeys {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String NEIGHBORHOOD = "neighborhood";
    private static final String ADDRESS = "address";
    private static final String CITY = "city";
    private static final String STATE = "state";
    private static final String POSTAL_CODE = "postalCode";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String STARS = "stars";
    private static final String REVIEW_COUNT = "reviewCount";
    private static final String IS_OPEN = "isOpen";
    private static final String COUNT_MIXED = "countMixed";
    private static final String COUNT_NEGATIVE = "countNegative";
    private static final String COUNT_NEUTRAL = "countNeutral";
    private static final String COUNT_POSITIVE = "countPositive";

  }

  public static class ColumnKeys {

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String NEIGHBORHOOD = "neighborhood";
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String POSTAL_CODE = "postal_code";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String STARS = "stars";
    public static final String REVIEW_COUNT = "review_count";
    public static final String IS_OPEN = "is_open";
    public static final String COUNT_MIXED = "count_mixed";
    public static final String COUNT_NEGATIVE = "count_negative";
    public static final String COUNT_NEUTRAL = "count_neutral";
    public static final String COUNT_POSITIVE = "count_positive";

  }

  @JsonProperty(JsonKeys.ID)
  @Column(name = ColumnKeys.ID)
  private String id;

  @JsonProperty(JsonKeys.NAME)
  @Column(name = ColumnKeys.NAME)
  private String name;

  @JsonProperty(JsonKeys.NEIGHBORHOOD)
  @Column(name = ColumnKeys.NEIGHBORHOOD)
  private String neighborhood;

  @JsonProperty(JsonKeys.ADDRESS)
  @Column(name = ColumnKeys.ADDRESS)
  private String address;

  @JsonProperty(JsonKeys.CITY)
  @Column(name = ColumnKeys.CITY)
  private String city;

  @JsonProperty(JsonKeys.STATE)
  @Column(name = ColumnKeys.STATE)
  private String state;

  @JsonProperty(JsonKeys.POSTAL_CODE)
  @Column(name = ColumnKeys.POSTAL_CODE)
  private String postalCode;

  @JsonProperty(JsonKeys.LATITUDE)
  @Column(name = ColumnKeys.LATITUDE)
  private Float latitude;

  @JsonProperty(JsonKeys.LONGITUDE)
  @Column(name = ColumnKeys.LONGITUDE)
  private Float longitude;

  @JsonProperty(JsonKeys.STARS)
  @Column(name = ColumnKeys.STARS)
  private Float stars;

  @JsonProperty(JsonKeys.REVIEW_COUNT)
  @Column(name = ColumnKeys.REVIEW_COUNT)
  private Long reviewCount;

  @JsonProperty(JsonKeys.IS_OPEN)
  @Column(name = ColumnKeys.IS_OPEN)
  private Boolean isOpen;

  @JsonProperty(JsonKeys.COUNT_MIXED)
  @Column(name = ColumnKeys.COUNT_MIXED)
  private Integer countMixed;

  @JsonProperty(JsonKeys.COUNT_NEGATIVE)
  @Column(name = ColumnKeys.COUNT_NEGATIVE)
  private Integer countNegative;

  @JsonProperty(JsonKeys.COUNT_NEUTRAL)
  @Column(name = ColumnKeys.COUNT_NEUTRAL)
  private Integer countNeutral;

  @JsonProperty(JsonKeys.COUNT_POSITIVE)
  @Column(name = ColumnKeys.COUNT_POSITIVE)
  private Integer countPositive;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNeighborhood() {
    return neighborhood;
  }

  public void setNeighborhood(String neighborhood) {
    this.neighborhood = neighborhood;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public Float getLatitude() {
    return latitude;
  }

  public void setLatitude(Float latitude) {
    this.latitude = latitude;
  }

  public Float getLongitude() {
    return longitude;
  }

  public void setLongitude(Float longitude) {
    this.longitude = longitude;
  }

  public Float getStars() {
    return stars;
  }

  public void setStars(Float stars) {
    this.stars = stars;
  }

  public Long getReviewCount() {
    return reviewCount;
  }

  public void setReviewCount(Long reviewCount) {
    this.reviewCount = reviewCount;
  }

  public Boolean getIsOpen() {
    return isOpen;
  }

  public void setIsOpen(Boolean isOpen) {
    this.isOpen = isOpen;
  }

  public Integer getCountMixed() {
    return countMixed;
  }

  public void setCountMixed(Integer countMixed) {
    this.countMixed = countMixed;
  }

  public Integer getCountNegative() {
    return countNegative;
  }

  public void setCountNegative(Integer countNegative) {
    this.countNegative = countNegative;
  }

  public Integer getCountNeutral() {
    return countNeutral;
  }

  public void setCountNeutral(Integer countNeutral) {
    this.countNeutral = countNeutral;
  }

  public Integer getCountPositive() {
    return countPositive;
  }

  public void setCountPositive(Integer countPositive) {
    this.countPositive = countPositive;
  }

}
