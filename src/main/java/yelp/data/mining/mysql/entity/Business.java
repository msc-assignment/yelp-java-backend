package yelp.data.mining.mysql.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "business")
public class Business {

  public static class ColumnKeys {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String NEIGHBORHOOD = "neighborhood";
    private static final String ADDRESS = "address";
    private static final String CITY = "city";
    private static final String STATE = "state";
    private static final String POSTAL_CODE = "postal_code";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String STARS = "stars";
    private static final String REVIEW_COUNT = "review_count";
    private static final String IS_OPEN = "is_open";
  }

  public static class JsonKeys {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String NEIGHBORHOOD = "neighborhood";
    private static final String ADDRESS = "address";
    private static final String CITY = "city";
    private static final String STATE = "state";
    private static final String POSTAL_CODE = "postalCode";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String STARS = "stars";
    private static final String REVIEW_COUNT = "reviewCount";
    private static final String IS_OPEN = "isOpen";
  }

  @Id
  @Column(name = ColumnKeys.ID)
  @JsonProperty(JsonKeys.ID)
  private String id;

  @Column(name = ColumnKeys.NAME)
  @JsonProperty(JsonKeys.NAME)
  private String name;

  @Column(name = ColumnKeys.NEIGHBORHOOD)
  @JsonProperty(JsonKeys.NEIGHBORHOOD)
  private String neighborhood;

  @Column(name = ColumnKeys.ADDRESS)
  @JsonProperty(JsonKeys.ADDRESS)
  private String address;

  @Column(name = ColumnKeys.CITY)
  @JsonProperty(JsonKeys.CITY)
  private String city;

  @Column(name = ColumnKeys.STATE)
  @JsonProperty(JsonKeys.STATE)
  private String state;

  @Column(name = ColumnKeys.POSTAL_CODE)
  @JsonProperty(JsonKeys.POSTAL_CODE)
  private String postalCode;

  @Column(name = ColumnKeys.LATITUDE)
  @JsonProperty(JsonKeys.LATITUDE)
  private Float latitude;

  @Column(name = ColumnKeys.LONGITUDE)
  @JsonProperty(JsonKeys.LONGITUDE)
  private Float longitude;

  @Column(name = ColumnKeys.STARS)
  @JsonProperty(JsonKeys.STARS)
  private Float stars;

  @Column(name = ColumnKeys.REVIEW_COUNT)
  @JsonProperty(JsonKeys.REVIEW_COUNT)
  private Long reviewCount;

  @Column(name = ColumnKeys.IS_OPEN)
  @JsonProperty(JsonKeys.IS_OPEN)
  private Boolean isOpen;

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = Review.ColumnKeys.BUSINESS_ID)
  @JsonIgnore
  private Set<Review> reviews;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNeighborhood() {
    return neighborhood;
  }

  public void setNeighborhood(String neighborhood) {
    this.neighborhood = neighborhood;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public Float getLatitude() {
    return latitude;
  }

  public void setLatitude(Float latitude) {
    this.latitude = latitude;
  }

  public Float getLongitude() {
    return longitude;
  }

  public void setLongitude(Float longitude) {
    this.longitude = longitude;
  }

  public Float getStars() {
    return stars;
  }

  public void setStars(Float stars) {
    this.stars = stars;
  }

  public Long getReviewCount() {
    return reviewCount;
  }

  public void setReviewCount(Long reviewCount) {
    this.reviewCount = reviewCount;
  }

  public Boolean getIsOpen() {
    return isOpen;
  }

  public void setIsOpen(Boolean isOpen) {
    this.isOpen = isOpen;
  }

  public Set<Review> getReviews() {
    return reviews;
  }

  public void setReviews(Set<Review> reviews) {
    this.reviews = reviews;
  }

}
