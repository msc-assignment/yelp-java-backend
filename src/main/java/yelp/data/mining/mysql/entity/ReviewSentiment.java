package yelp.data.mining.mysql.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "review_sentiment")
public class ReviewSentiment {

  public static class ColumnKeys {

    private static final String ID = "id";
    private static final String REVIEW_ID = "review_id";
    private static final String BUSINESS_ID = "business_id";
    private static final String SENTIMENT = "sentiment";
    private static final String MIXED_SCORE = "mixed_score";
    private static final String NEGATIVE_SCORE = "negative_score";
    private static final String NEUTRAL_SCORE = "neutral_score";
    private static final String POSITIVE_SCORE = "positive_score";
  }

  public static class JsonKeys {

    private static final String ID = "id";
    private static final String REVIEW_ID = "reviewId";
    private static final String SENTIMENT = "sentiment";
    private static final String MIXED_SCORE = "mixed_score";
    private static final String NEGATIVE_SCORE = "negative_score";
    private static final String NEUTRAL_SCORE = "neutral_score";
    private static final String POSITIVE_SCORE = "positive_score";
  }

  @Id
  @Column(name = ColumnKeys.ID)
  @GeneratedValue
  @JsonProperty(JsonKeys.ID)
  private Long id;

  @Column(name = ColumnKeys.REVIEW_ID)
  @JsonProperty(JsonKeys.REVIEW_ID)
  private String reviewId;

  @Column(name = ColumnKeys.SENTIMENT)
  @JsonProperty(JsonKeys.SENTIMENT)
  private String sentiment;

  @Column(name = ColumnKeys.MIXED_SCORE)
  @JsonProperty(JsonKeys.MIXED_SCORE)
  private Float mixedScore;

  @Column(name = ColumnKeys.NEGATIVE_SCORE)
  @JsonProperty(JsonKeys.NEGATIVE_SCORE)
  private Float negativeScore;

  @Column(name = ColumnKeys.NEUTRAL_SCORE)
  @JsonProperty(JsonKeys.NEUTRAL_SCORE)
  private Float neutralScore;

  @Column(name = ColumnKeys.POSITIVE_SCORE)
  @JsonProperty(JsonKeys.POSITIVE_SCORE)
  private Float positiveScore;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = ColumnKeys.BUSINESS_ID)
  @JsonIgnore
  private Business business;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getReviewId() {
    return reviewId;
  }

  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }

  public String getSentiment() {
    return sentiment;
  }

  public void setSentiment(String sentiment) {
    this.sentiment = sentiment;
  }

  public Float getMixedScore() {
    return mixedScore;
  }

  public void setMixedScore(Float mixedScore) {
    this.mixedScore = mixedScore;
  }

  public Float getNegativeScore() {
    return negativeScore;
  }

  public void setNegativeScore(Float negativeScore) {
    this.negativeScore = negativeScore;
  }

  public Float getNeutralScore() {
    return neutralScore;
  }

  public void setNeutralScore(Float neutralScore) {
    this.neutralScore = neutralScore;
  }

  public Float getPositiveScore() {
    return positiveScore;
  }

  public void setPositiveScore(Float positiveScore) {
    this.positiveScore = positiveScore;
  }

  public Business getBusiness() {
    return business;
  }

  public void setBusiness(Business business) {
    this.business = business;
  }

}
