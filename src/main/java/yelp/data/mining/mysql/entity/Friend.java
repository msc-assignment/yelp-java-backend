package yelp.data.mining.mysql.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import yelp.data.mining.mysql.entity.id.FriendId;

@Entity
@Table(name = "friend")
@IdClass(FriendId.class)
public class Friend {

  public static class ColumnKeys {

    public static final String USER_ID = "user_id";
    private static final String FRIEND_ID = "friend_id";
  }

  public static class JsonKeys {

    private static final String FRIEND_ID = "friendId";
    private static final String USER_ID = "userId";
  }

  @Id
  @Column(name = ColumnKeys.FRIEND_ID)
  @JsonProperty(JsonKeys.FRIEND_ID)
  private String friendId;

  @Id
  @Column(name = ColumnKeys.USER_ID)
  @JsonProperty(JsonKeys.USER_ID)
  private String userId;

  public String getFriendId() {
    return friendId;
  }

  public void setFriendId(String friendId) {
    this.friendId = friendId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

}
