package yelp.data.mining;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.comprehend.AmazonComprehend;
import com.amazonaws.services.comprehend.AmazonComprehendClientBuilder;
import opennlp.tools.stemmer.PorterStemmer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;
import yelp.data.mining.config.ApplicationConfiguration;

@SpringBootApplication
@EnableAsync
@EnableNeo4jRepositories
public class MainApplication {

  private final static Logger log = LoggerFactory.getLogger(MainApplication.class);

  public static void main(String[] args) throws Exception {
    SpringApplication.run(MainApplication.class, args);
  }

  @Bean
  public RestTemplate getRestTemplate() {
    return new RestTemplate();
  }

  @Bean
  public PorterStemmer getPorterStemmer() {
    return new PorterStemmer();
  }

  @Autowired
  private ApplicationConfiguration config;

  @Bean
  public AmazonComprehend getAmazonComprehend() {
    final AWSStaticCredentialsProvider auth
            = new AWSStaticCredentialsProvider(
                    new BasicAWSCredentials(config.getAwsAccessKey(), config.getAwsSecretKey()));

    final AmazonComprehend client = AmazonComprehendClientBuilder
            .standard()
            .withCredentials(auth)
            .withRegion(config.getAwsRegion())
            .build();

    return client;
  }

}
