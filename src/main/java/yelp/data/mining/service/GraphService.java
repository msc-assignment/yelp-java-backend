package yelp.data.mining.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yelp.data.mining.mysql.entity.Friend;
import yelp.data.mining.mysql.entity.ReviewSentiment;
import yelp.data.mining.mysql.entity.User;
import yelp.data.mining.mysql.repository.FriendRepository;
import yelp.data.mining.neo4j.entity.BusinessGraph;
import yelp.data.mining.neo4j.entity.BusinessKeywordRelationship;
import yelp.data.mining.neo4j.entity.KeywordGraph;
import yelp.data.mining.neo4j.entity.UserGraph;
import yelp.data.mining.neo4j.entity.UserKeywordRelationship;
import yelp.data.mining.neo4j.entity.UserRelationship;
import yelp.data.mining.neo4j.repository.BusinessGraphRepository;
import yelp.data.mining.neo4j.repository.KeywordGraphRepository;
import yelp.data.mining.neo4j.repository.UserGraphRepository;

@Service
public class GraphService {

  @Autowired
  private KeywordGraphRepository keywordGraphRepository;

  @Autowired
  private BusinessGraphRepository businessGraphRepository;

  @Autowired
  private UserGraphRepository userGraphRepository;

  @Autowired
  private FriendRepository friendRepository;

  @Transactional
  public BusinessGraph generateBusinessGraph(final String businessId, final String uuid) {

    BusinessGraph businessGraph
            = this.businessGraphRepository.findByBusinessId(businessId);

    if (businessGraph != null) {
      return businessGraph;
    }

    businessGraph = new BusinessGraph(businessId);
    return this.businessGraphRepository.save(businessGraph);
  }

  @Transactional
  public UserGraph generateUserGraph(final User user, final String uuid) {

    UserGraph userGraph = this.userGraphRepository.findByUserId(user.getId());

    if (userGraph == null) {
      userGraph = new UserGraph(user.getId());
    }

    List<Friend> friends = this.friendRepository.findByUserId(user.getId());

    if (friends != null && !friends.isEmpty()) {
      for (Friend friend : friends) {

        UserGraph friendGraph = this.userGraphRepository.findByUserId(friend.getFriendId());

        if (friendGraph == null) {
          friendGraph = new UserGraph(friend.getFriendId());
          friendGraph = this.userGraphRepository.save(friendGraph);
        }

        final UserRelationship userRelationship = new UserRelationship();
        userRelationship.setUser1(userGraph);
        userRelationship.setUser2(friendGraph);

        userGraph.getFriends().add(userRelationship);
      }
    }

    return this.userGraphRepository.save(userGraph);
  }

  @Transactional
  public void generateRelationship(final BusinessGraph businessGraph, final UserGraph userGraph,
          final String reviewId, final String keyword,
          final ReviewSentiment reviewSentiment, final Boolean isSynonym) {

    final String prefix;

    switch (reviewSentiment.getSentiment().toLowerCase()) {
      case "neutral":
        prefix = "~";
        break;
      case "positive":
        prefix = "+";
        break;
      case "mixed":
        prefix = "!";
        break;
      case "negative":
        prefix = "-";
        break;
      default:
        prefix = "";
        break;
    }

    KeywordGraph keywordGraph
            = this.keywordGraphRepository.findByKeyword(prefix.concat(keyword));

    if (keywordGraph == null) {
      keywordGraph = new KeywordGraph(prefix.concat(keyword), reviewId, isSynonym);
    }

    boolean isBusinessKeywordRelationshipFound = false;

    for (BusinessKeywordRelationship businessKeywordRelationship
            : keywordGraph.getBusinessKeywords()) {

      if (businessKeywordRelationship.getBusiness().getBusinessId().equalsIgnoreCase(businessGraph.getBusinessId())) {
        isBusinessKeywordRelationshipFound = true;
        businessKeywordRelationship.setWeight(businessKeywordRelationship.getWeight() + 1);
        break;
      }
    }

    if (!isBusinessKeywordRelationshipFound) {
      final BusinessKeywordRelationship businessKeywordRelation
              = new BusinessKeywordRelationship(businessGraph, keywordGraph, 1);

      businessGraph.getBusinessKeywords().add(businessKeywordRelation);
      keywordGraph.getBusinessKeywords().add(businessKeywordRelation);
    }

    boolean isUserKeywordRelationshipFound = false;

    for (UserKeywordRelationship userKeywordRelationship
            : keywordGraph.getUserKeywords()) {

      if (userKeywordRelationship.getUser().getUserId().equalsIgnoreCase(userGraph.getUserId())) {
        isUserKeywordRelationshipFound = true;
        userKeywordRelationship.setWeight(userKeywordRelationship.getWeight() + 1);
        break;
      }
    }

    if (!isUserKeywordRelationshipFound) {
      final UserKeywordRelationship userKeywordRelation
              = new UserKeywordRelationship(userGraph, keywordGraph, 1);

      userGraph.getUserKeywords().add(userKeywordRelation);
      keywordGraph.getUserKeywords().add(userKeywordRelation);
    }

    keywordGraphRepository.save(keywordGraph);
  }

}
