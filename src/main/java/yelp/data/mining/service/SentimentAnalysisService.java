package yelp.data.mining.service;

import com.amazonaws.services.comprehend.AmazonComprehend;
import com.amazonaws.services.comprehend.model.DetectSentimentRequest;
import com.amazonaws.services.comprehend.model.DetectSentimentResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SentimentAnalysisService {
  
  @Autowired
  public AmazonComprehend amazonComprehend;
  
  public DetectSentimentResult extractSentimentAnalysis(final String text) {
    
    final DetectSentimentRequest request = new DetectSentimentRequest();
    request.setText(text);
    request.setLanguageCode("en");
    
    return amazonComprehend.detectSentiment(request);
  }
  
}
