package yelp.data.mining.service;

import com.amazonaws.services.comprehend.model.DetectSentimentResult;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import opennlp.tools.stemmer.PorterStemmer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import yelp.data.mining.config.ApplicationConfiguration;
import yelp.data.mining.mysql.entity.Business;
import yelp.data.mining.mysql.entity.BusinessReview;
import yelp.data.mining.mysql.entity.BusinessSimple;
import yelp.data.mining.mysql.entity.Review;
import yelp.data.mining.mysql.entity.ReviewSentiment;
import yelp.data.mining.mysql.entity.User;
import yelp.data.mining.mysql.repository.BusinessRepository;
import yelp.data.mining.mysql.repository.BusinessSimpleRepository;
import yelp.data.mining.mysql.repository.ReviewRepository;
import yelp.data.mining.mysql.repository.ReviewSentimentRepository;
import yelp.data.mining.mysql.repository.UserRepository;
import yelp.data.mining.neo4j.entity.BusinessGraph;
import yelp.data.mining.neo4j.entity.BusinessKeywordRelationship;
import yelp.data.mining.neo4j.entity.KeywordGraph;
import yelp.data.mining.neo4j.entity.UserGraph;
import yelp.data.mining.neo4j.repository.KeywordGraphRepository;

@Service
public class DataMiningService {

  @Autowired
  private ReviewRepository reviewRepository;

  @Autowired
  private KeywordGraphRepository keywordGraphRepository;

  @Autowired
  private BusinessRepository businessRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ReviewSentimentRepository reviewSentimentRepository;

  @Autowired
  private BusinessSimpleRepository businessSimpleRepository;

  @Autowired
  private StopWordsService stopWordsService;

  @Autowired
  private SentimentAnalysisService sentimentAnalysisService;

  @Autowired
  private PorterStemmer stemmer;

  @Autowired
  private ThesaurusService thesaurusService;

  @Autowired
  private GraphService graphService;

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Autowired
  private ApplicationConfiguration config;

  private static final Logger LOGGER
          = LoggerFactory.getLogger(DataMiningService.class);

  public List<Business> getAllBusiness(final String uuid) {
    LOGGER.info("UUID: [{}] Retrieving list of Businesses", uuid);
    return this.businessRepository.findAll();
  }

  public Business getBusiness(final String businessId, final String uuid) {
    LOGGER.info("UUID: [{}] Get Business [{}]", uuid, businessId);
    return this.businessRepository.findOne(businessId);
  }

  public List<BusinessSimple> getAllBusinessSimple(final String uuid) {
    LOGGER.info("UUID: [{}] Retrieving list of Simple Businesses", uuid);
    return this.businessSimpleRepository.findAll();
  }

  public Page<Business> getAllBusiness(final Pageable page, final String uuid) {
    LOGGER.info("UUID: [{}] Retrieving list of Businesses by page", uuid);
    return this.businessRepository.findAll(page);
  }

  public List<User> getAllUsers(final String uuid) {
    LOGGER.info("UUID: [{}] Retrieving list of Users", uuid);
    return this.userRepository.findAll();
  }

  public Page<User> getAllUsers(final Pageable page, final String uuid) {
    LOGGER.info("UUID: [{}] Retrieving list of Users by page", uuid);
    return this.userRepository.findAll(page);
  }

  public List<Business> getBusinessByKeyword(final String keyword, final String uuid) {
    final String stemmedWord = stemmer.stem(keyword);
    LOGGER.info("UUID: [{}] Retrieving businesses with keyword [{}] which is stemmed to [{}]", uuid, keyword, stemmedWord);
    final KeywordGraph keywordGraph = keywordGraphRepository.findByKeyword(stemmedWord);

    if (keywordGraph == null) {
      return new ArrayList<>();
    }

    final List<String> businessIds = new ArrayList<>();

    for (BusinessKeywordRelationship rel : keywordGraph.getBusinessKeywords()) {
      LOGGER.info("UUID: [{}] Adding businessId [{}]", uuid, rel.getBusiness().getBusinessId());
      businessIds.add(rel.getBusiness().getBusinessId());
    }

    return this.businessRepository.findByIdIn(businessIds);
  }

  public List<BusinessReview> getReviewSentimentsByLatitudeAndLongitude(final Double latitude,
          final Double longitude, final Double radius, final String uuid) {
    LOGGER.info("UUID: [{}] Retrieving businesses with radius [{}] for latitude "
            + "[{}] and longitude [{}]", uuid, radius, latitude, longitude);

    final String rawQuery = String.format("SELECT \n"
            + "    b.id,\n"
            + "    b.name,\n"
            + "    b.neighborhood,\n"
            + "    b.address,\n"
            + "    b.city,\n"
            + "    b.state,\n"
            + "    b.postal_code,\n"
            + "    b.latitude,\n"
            + "    b.longitude,\n"
            + "    b.stars,\n"
            + "    b.review_count,\n"
            + "    b.is_open,\n"
            + "    (SELECT COUNT(1) FROM review_sentiment WHERE business_id = b.id AND sentiment = \"POSITIVE\") as 'count_positive', \n"
            + "    (SELECT COUNT(1) FROM review_sentiment WHERE business_id = b.id AND sentiment = \"NEUTRAL\") as 'count_neutral', \n"
            + "    (SELECT COUNT(1) FROM review_sentiment WHERE business_id = b.id AND sentiment = \"NEGATIVE\") as 'count_negative', \n"
            + "    (SELECT COUNT(1) FROM review_sentiment WHERE business_id = b.id AND sentiment = \"MIXED\") as 'count_mixed', "
            + "    (6371 * ACOS(COS(RADIANS(%s)) * COS(RADIANS(b.latitude)) * "
            + "COS(RADIANS(b.longitude) - RADIANS(%s)) + SIN(RADIANS(%s)) * "
            + "SIN(RADIANS(b.latitude)))) AS distance\n"
            + "FROM\n"
            + "    yelp_min.business b,\n"
            + "    yelp_min.review_sentiment rs\n"
            + "WHERE\n"
            + "    b.id = rs.business_id\n"
            + "GROUP BY b.id\n"
            + "HAVING distance < %s\n"
            + "ORDER BY distance  \n", latitude, longitude, latitude, radius
    );

    final List<BusinessReview> reviews = this.jdbcTemplate.query(rawQuery, new RowMapper<BusinessReview>() {
      @Override
      public BusinessReview mapRow(ResultSet rs, int i) throws SQLException {

        final BusinessReview businessReview = new BusinessReview();

        businessReview.setId(rs.getString(BusinessReview.ColumnKeys.ID));
        businessReview.setAddress(rs.getString(BusinessReview.ColumnKeys.ADDRESS));
        businessReview.setCity(rs.getString(BusinessReview.ColumnKeys.CITY));
        businessReview.setCountMixed(rs.getInt(BusinessReview.ColumnKeys.COUNT_MIXED));
        businessReview.setCountNegative(rs.getInt(BusinessReview.ColumnKeys.COUNT_NEGATIVE));
        businessReview.setCountNeutral(rs.getInt(BusinessReview.ColumnKeys.COUNT_NEUTRAL));
        businessReview.setCountPositive(rs.getInt(BusinessReview.ColumnKeys.COUNT_POSITIVE));
        businessReview.setIsOpen(rs.getBoolean(BusinessReview.ColumnKeys.IS_OPEN));
        businessReview.setLatitude(rs.getFloat(BusinessReview.ColumnKeys.LATITUDE));
        businessReview.setLongitude(rs.getFloat(BusinessReview.ColumnKeys.LONGITUDE));
        businessReview.setName(rs.getString(BusinessReview.ColumnKeys.NAME));
        businessReview.setNeighborhood(rs.getString(BusinessReview.ColumnKeys.NEIGHBORHOOD));
        businessReview.setPostalCode(rs.getString(BusinessReview.ColumnKeys.NEIGHBORHOOD));
        businessReview.setReviewCount(rs.getLong(BusinessReview.ColumnKeys.REVIEW_COUNT));
        businessReview.setStars(rs.getFloat(BusinessReview.ColumnKeys.STARS));
        businessReview.setState(rs.getString(BusinessReview.ColumnKeys.STATE));

        return businessReview;
      }
    });

    return reviews;
  }

  public void indexReviews(final Integer limit, final String uuid) {

    LOGGER.info("UUID: [{}] Retrieving reviews with processed = [false] and "
            + "limit [{}]", uuid, limit);

    final Pageable pageable = new PageRequest(0, limit);

    final Page<Review> reviews = this.reviewRepository.findByIsProcessed(false, pageable);

    for (Review review : reviews.getContent()) {
      try {
        indexReview(review, uuid);
      } catch (Exception ex) {
        LOGGER.error("UUID: [{}] Exception Encountered -> [{}]", uuid, ex);
        try {
          indexReview(review, uuid);
        } catch (Exception ex2) {
          LOGGER.error("UUID: [{}] Another Exception Encountered -> [{}].", uuid, ex2);
          LOGGER.error("UUID: [{}] Ignoring ReviewId -> [{}].", uuid, review.getId());
        }
      }
    }
  }

  public void indexReview(final Review review, final String uuid) {

    LOGGER.info("UUID: [{}] Starting indexing Review ID [{}]", uuid,
            review.getId());

    System.gc();

    final ReviewSentiment reviewSentiment = storeReviewSentiment(review, uuid);

    final BusinessGraph businessGraph = graphService.generateBusinessGraph(
            review.getBusiness().getId(), uuid);

    final UserGraph userGraph = graphService.generateUserGraph(
            review.getUser(), uuid);

    LOGGER.info("UUID: [{}] Splitting text [{}]", uuid, review.getText());

    final String[] words = review.getText().split(" ");

    for (String word : words) {

      final String filteredText = word.replaceAll("[^a-zA-Z ]", "").toLowerCase();
      LOGGER.info("UUID: [{}] Removed punctuations from [{}] to [{}]", uuid, word, filteredText);

      if (stopWordsService.isStopWord(filteredText) || filteredText.isEmpty()) {
        LOGGER.info("UUID: [{}] Word [{}] is a stop word", uuid, filteredText);
      } else {
        final String stemmedWord = stemmer.stem(filteredText);
        LOGGER.info("UUID: [{}] Word [{}] stemmed to [{}]", uuid, filteredText, stemmedWord);

        LOGGER.info("UUID: [{}] Indexing word [{}]", uuid, stemmedWord);
        graphService.generateRelationship(businessGraph, userGraph, review.getId(),
                stemmedWord, reviewSentiment, false);

        processSynonms(businessGraph, userGraph, stemmedWord, review, reviewSentiment, uuid);
      }
    }

    review.setIsProcessed(true);
    reviewRepository.save(review);
    System.gc();
  }

  private void processSynonms(final BusinessGraph businessGraph,
          final UserGraph userGraph, final String word, final Review review,
          final ReviewSentiment reviewSentiment, final String uuid) {

    if (!config.getUseSynonyms()) {
      return;
    }

    LOGGER.info("UUID: [{}] Getting Thesauri of word [{}]", uuid, word);
    final List<String> otherWords = this.thesaurusService.getOtherThesauri(word);

    if (otherWords != null && !otherWords.isEmpty()) {
      for (String otherWord : otherWords) {
        LOGGER.info("UUID: [{}] Indexing synonym [{}] of original word [{}]",
                uuid, otherWord, word);

        final String[] synonymsSplit = otherWord.replaceAll("[^a-zA-Z ]", "").toLowerCase().split(" ");

        for (String synonym : synonymsSplit) {
          final String synonymWord = stemmer.stem(synonym);
          graphService.generateRelationship(businessGraph, userGraph, review.getId(),
                  synonymWord, reviewSentiment, false);

        }
      }
    }
  }

  public void generateSentimentAnalysis(Pageable pageable, final String uuid) {

    Page<Review> reviews = null;

    do {
      reviews = this.reviewRepository.findAll(pageable);

      for (Review review : reviews) {
        storeReviewSentiment(review, uuid);
      }
      pageable = new PageRequest(pageable.getPageNumber() + 1, pageable.getPageSize());

    } while (reviews != null && reviews.hasContent());

  }

  private ReviewSentiment storeReviewSentiment(final Review review, final String uuid) {

    LOGGER.info("UUID: [{}] Extracting Sentiment for Review ID [{}].", uuid, review.getId());

    final DetectSentimentResult result
            = sentimentAnalysisService.extractSentimentAnalysis(review.getText());

    final ReviewSentiment reviewSentiment = new ReviewSentiment();

    reviewSentiment.setBusiness(review.getBusiness());
    reviewSentiment.setReviewId(review.getId());
    reviewSentiment.setMixedScore(result.getSentimentScore().getMixed());
    reviewSentiment.setPositiveScore(result.getSentimentScore().getPositive());
    reviewSentiment.setNeutralScore(result.getSentimentScore().getNeutral());
    reviewSentiment.setNegativeScore(result.getSentimentScore().getNegative());
    reviewSentiment.setSentiment(result.getSentiment());

    return this.reviewSentimentRepository.save(reviewSentiment);
  }

}
