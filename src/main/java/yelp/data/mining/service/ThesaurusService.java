package yelp.data.mining.service;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import yelp.data.mining.json.SynonymsResponse;

@Service
public class ThesaurusService {

  @Autowired
  private RestTemplate restTemplate;

  public List<String> getOtherThesauri(final String word) {

    final String uri = String.format("https://wordsapiv1.p.mashape.com/words/%s/synonyms", word);

    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
    headers.add("X-Mashape-Key", "SxlzEp4b1Omsh6ZxzmcHb4PlOe7gp1jl9pUjsnM6oASOrXBaK0");
    headers.add("X-Mashape-Host", "wordsapiv1.p.mashape.com");

    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
    try {

      final ResponseEntity<SynonymsResponse> response
              = restTemplate.exchange(uri, HttpMethod.GET,
                      entity, SynonymsResponse.class);
      return response.getBody().getSynonyms();

    } catch (HttpClientErrorException ex) {

      return null;
    }

  }
}
