package yelp.data.mining.controller;

import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import yelp.data.mining.mysql.entity.Business;
import yelp.data.mining.mysql.entity.BusinessReview;
import yelp.data.mining.mysql.entity.BusinessSimple;
import yelp.data.mining.mysql.entity.User;
import yelp.data.mining.service.DataMiningService;

@RestController
@RequestMapping(path = "")
public class MainController {

  @Autowired
  private DataMiningService dataMiningService;

  @GetMapping("")
  public void indexReviews(final @RequestParam Integer limit) {

    final String uuid = UUID.randomUUID().toString();

    dataMiningService.indexReviews(limit, uuid);
  }

  @GetMapping("sentiment")
  public void sentiment(final Pageable pageable) {

    final String uuid = UUID.randomUUID().toString();

    dataMiningService.generateSentimentAnalysis(pageable, uuid);
  }

  @GetMapping("location")
  public ResponseEntity<List<BusinessReview>> getReviewSentimentsByLocation(
          final @RequestParam Double latitude, final @RequestParam Double longitude,
          final @RequestParam(defaultValue = "10") Double radius) {

    final String uuid = UUID.randomUUID().toString();

    return ResponseEntity.ok(dataMiningService.getReviewSentimentsByLatitudeAndLongitude(latitude, longitude, radius, uuid));
  }

  @GetMapping("search")
  public ResponseEntity<List<Business>> searchBusinessByKeyword(
          final @RequestParam String keyword) {

    final String uuid = UUID.randomUUID().toString();

    return ResponseEntity.ok(dataMiningService.getBusinessByKeyword(keyword, uuid));
  }

  @GetMapping("business")
  public ResponseEntity<List<Business>> getAllBusiness() {

    final String uuid = UUID.randomUUID().toString();

    return ResponseEntity.ok(dataMiningService.getAllBusiness(uuid));
  }

  @GetMapping("business/simple")
  public ResponseEntity<List<BusinessSimple>> getAllBusinessSimple() {

    final String uuid = UUID.randomUUID().toString();

    return ResponseEntity.ok(dataMiningService.getAllBusinessSimple(uuid));
  }

  @GetMapping("business/id/{businessId}")
  public ResponseEntity<Business> getBusiness(
          final @PathVariable("businessId") String businessId) {

    final String uuid = UUID.randomUUID().toString();

    return ResponseEntity.ok(dataMiningService.getBusiness(businessId, uuid));
  }

  @GetMapping("user")
  public ResponseEntity<List<User>> getAllUsers() {

    final String uuid = UUID.randomUUID().toString();

    return ResponseEntity.ok(dataMiningService.getAllUsers(uuid));
  }

  @GetMapping("business/page")
  public ResponseEntity<Page<Business>> getAllBusiness(final Pageable page) {

    final String uuid = UUID.randomUUID().toString();

    return ResponseEntity.ok(dataMiningService.getAllBusiness(page, uuid));
  }

  @GetMapping("user/page")
  public ResponseEntity<Page<User>> getAllUsers(final Pageable page) {

    final String uuid = UUID.randomUUID().toString();

    return ResponseEntity.ok(dataMiningService.getAllUsers(page, uuid));
  }
}
