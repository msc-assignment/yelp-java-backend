package yelp.data.mining.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class SynonymsResponse {

  public static class JsonKeys {

    private static final String WORD = "word";
    private static final String SYNONYMS = "synonyms";
  }

  @JsonProperty(JsonKeys.WORD)
  private String word;

  @JsonProperty(JsonKeys.SYNONYMS)
  private List<String> synonyms;

  public String getWord() {
    return word;
  }

  public List<String> getSynonyms() {
    return synonyms;
  }

}
