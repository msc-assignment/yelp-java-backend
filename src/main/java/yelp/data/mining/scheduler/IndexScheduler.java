package yelp.data.mining.scheduler;

import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import yelp.data.mining.service.DataMiningService;

public class IndexScheduler {

  @Autowired
  private DataMiningService dataMiningService;

  private static final Logger log = LoggerFactory.getLogger(IndexScheduler.class);

  public void refreshCache() {

    final String uuid = UUID.randomUUID().toString();

    dataMiningService.indexReviews(1, uuid);
  }

}
