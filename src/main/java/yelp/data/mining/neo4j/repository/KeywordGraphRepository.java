package yelp.data.mining.neo4j.repository;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import yelp.data.mining.neo4j.entity.KeywordGraph;

@RepositoryRestResource(exported = false)
public interface KeywordGraphRepository extends GraphRepository<KeywordGraph> {

  public KeywordGraph findByKeyword(String keyword);

}
