package yelp.data.mining.neo4j.repository;

import org.springframework.data.neo4j.repository.GraphRepository;
import yelp.data.mining.neo4j.entity.BusinessGraph;

public interface BusinessGraphRepository extends GraphRepository<BusinessGraph> {

  public BusinessGraph findByBusinessId(String businessId);

}
