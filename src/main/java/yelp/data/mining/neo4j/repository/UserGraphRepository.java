package yelp.data.mining.neo4j.repository;

import org.springframework.data.neo4j.repository.GraphRepository;
import yelp.data.mining.neo4j.entity.UserGraph;

public interface UserGraphRepository extends GraphRepository<UserGraph> {

  public UserGraph findByUserId(String userId);
}
