package yelp.data.mining.neo4j.entity;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = "USER_KEYWORDS")
public class UserKeywordRelationship {

  @GraphId
  private Long id;

  @StartNode
  private UserGraph user;

  @EndNode
  private KeywordGraph keyword;

  private Integer weight;

  public UserKeywordRelationship() {
  }

  public UserKeywordRelationship(UserGraph user, KeywordGraph keyword, Integer weight) {
    this.user = user;
    this.keyword = keyword;
    this.weight = weight;
  }

  public Long getId() {
    return id;
  }

  public UserGraph getUser() {
    return user;
  }

  public void setUser(UserGraph user) {
    this.user = user;
  }

  public KeywordGraph getKeyword() {
    return keyword;
  }

  public void setKeyword(KeywordGraph keyword) {
    this.keyword = keyword;
  }

  public Integer getWeight() {
    return weight;
  }

  public void setWeight(Integer weight) {
    this.weight = weight;
  }

}
