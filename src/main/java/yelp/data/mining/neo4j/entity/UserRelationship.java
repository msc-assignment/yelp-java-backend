package yelp.data.mining.neo4j.entity;

import java.io.Serializable;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = "FRIENDS")
public class UserRelationship implements Serializable {

  @GraphId
  private Long id;

  @StartNode
  private UserGraph user1;

  @EndNode
  private UserGraph user2;

  public UserRelationship() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public UserGraph getUser1() {
    return user1;
  }

  public void setUser1(UserGraph user1) {
    this.user1 = user1;
  }

  public UserGraph getUser2() {
    return user2;
  }

  public void setUser2(UserGraph user2) {
    this.user2 = user2;
  }

}
