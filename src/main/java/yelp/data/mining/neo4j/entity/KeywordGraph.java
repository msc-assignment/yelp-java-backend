package yelp.data.mining.neo4j.entity;

import java.util.ArrayList;
import java.util.List;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity(label = "KEYWORD")
public class KeywordGraph {

  @GraphId
  private Long id;

  @Index(unique = true)
  private String keyword;

  private String reviewId;

  private Boolean isSynonym;

  @Relationship(type = "BUSINESS_KEYWORDS", direction = Relationship.INCOMING)
  private List<BusinessKeywordRelationship> businessKeywords = new ArrayList<>();

  @Relationship(type = "USER_KEYWORDS", direction = Relationship.INCOMING)
  private List<UserKeywordRelationship> userKeywords = new ArrayList<>();

  public KeywordGraph() {
  }

  public KeywordGraph(String keyword, String reviewId, Boolean isSynonym) {
    this.keyword = keyword;
    this.reviewId = reviewId;
    this.isSynonym = isSynonym;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public String getReviewId() {
    return reviewId;
  }

  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }

  public Boolean getIsSynonym() {
    return isSynonym;
  }

  public void setIsSynonym(Boolean isSynonym) {
    this.isSynonym = isSynonym;
  }

  public List<BusinessKeywordRelationship> getBusinessKeywords() {
    return businessKeywords;
  }

  public void setBusinessKeywords(List<BusinessKeywordRelationship> businessKeywords) {
    this.businessKeywords = businessKeywords;
  }

  public List<UserKeywordRelationship> getUserKeywords() {
    return userKeywords;
  }

  public void setUserKeywords(List<UserKeywordRelationship> userKeywords) {
    this.userKeywords = userKeywords;
  }

}
