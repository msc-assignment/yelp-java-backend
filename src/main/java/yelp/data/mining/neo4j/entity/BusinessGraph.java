package yelp.data.mining.neo4j.entity;

import java.util.ArrayList;
import java.util.List;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity(label = "BUSINESS")
public class BusinessGraph {

  @GraphId
  private Long id;

  @Index(unique = true)
  private String businessId;

  @Relationship(type = "BUSINESS_KEYWORDS", direction = Relationship.OUTGOING)
  private List<BusinessKeywordRelationship> businessKeywords = new ArrayList<>();

  private BusinessGraph() {
  }

  public BusinessGraph(String businessId) {
    this.businessId = businessId;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBusinessId() {
    return businessId;
  }

  public void setBusinessId(String businessId) {
    this.businessId = businessId;
  }

  public List<BusinessKeywordRelationship> getBusinessKeywords() {
    return businessKeywords;
  }

  public void setBusinessKeywords(List<BusinessKeywordRelationship> businessKeywords) {
    this.businessKeywords = businessKeywords;
  }

}
