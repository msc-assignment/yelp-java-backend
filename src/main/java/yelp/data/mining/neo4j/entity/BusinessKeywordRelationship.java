package yelp.data.mining.neo4j.entity;

import java.io.Serializable;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = "BUSINESS_KEYWORDS")
public class BusinessKeywordRelationship implements Serializable {

  @GraphId
  private Long id;

  @StartNode
  private BusinessGraph business;

  @EndNode
  private KeywordGraph keyword;

  @Property
  private Integer weight;

  public BusinessKeywordRelationship() {
  }

  public BusinessKeywordRelationship(BusinessGraph business, KeywordGraph keyword, Integer weight) {
    this.business = business;
    this.keyword = keyword;
    this.weight = weight;
  }

  public Long getId() {
    return id;
  }

  public BusinessGraph getBusiness() {
    return business;
  }

  public void setBusiness(BusinessGraph business) {
    this.business = business;
  }

  public KeywordGraph getKeyword() {
    return keyword;
  }

  public void setKeyword(KeywordGraph keyword) {
    this.keyword = keyword;
  }

  public Integer getWeight() {
    return weight;
  }

  public void setWeight(Integer weight) {
    this.weight = weight;
  }

}
