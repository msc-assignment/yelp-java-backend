package yelp.data.mining.neo4j.entity;

import java.util.HashSet;
import java.util.Set;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity(label = "USER")
public class UserGraph {

  @GraphId
  private Long id;

  @Index(unique = true)
  private String userId;

  @Relationship(type = "USER_KEYWORDS", direction = Relationship.OUTGOING)
  private Set<UserKeywordRelationship> userKeywords;

  @Relationship(type = "FRIENDS", direction = Relationship.UNDIRECTED)
  private Set<UserRelationship> friends;

  private UserGraph() {
  }

  public UserGraph(String userId) {
    this.userId = userId;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Set<UserKeywordRelationship> getUserKeywords() {
    if (userKeywords == null) {
      userKeywords = new HashSet<>();
    }

    return userKeywords;
  }

  public void setUserKeywords(Set<UserKeywordRelationship> userKeywords) {
    this.userKeywords = userKeywords;
  }

  public Set<UserRelationship> getFriends() {
    if (friends == null) {
      friends = new HashSet<>();
    }

    return friends;
  }

  public void setFriends(Set<UserRelationship> friends) {
    this.friends = friends;
  }

}
