package yelp.data.mining.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

  @Value("${datamining.useSynonyms:false}")
  private Boolean useSynonyms;

  @Value("${datamining.amazon.comprehend.awsAccessKey}")
  private String awsAccessKey;

  @Value("${datamining.amazon.comprehend.awsSecretKey}")
  private String awsSecretKey;

  @Value("${datamining.amazon.comprehend.awsRegion}")
  private String awsRegion;

  public Boolean getUseSynonyms() {
    return useSynonyms;
  }

  public String getAwsAccessKey() {
    return awsAccessKey;
  }

  public String getAwsSecretKey() {
    return awsSecretKey;
  }

  public String getAwsRegion() {
    return awsRegion;
  }

}
